<?php

namespace Controllers;

use Core\Controller;

class MainController extends Controller {

    public function index() {
        require_once APP . '/view/main';
    }
}
