<?php

//use Core\App;
//
//require_once "../vendor/autoload.php";
//
define('BASE_DIR', str_replace('\\', '/', dirname(__DIR__)));
define('APP',  BASE_DIR . "/app/");
//
//$app = new App();
//
//define('PUBLIC_ROOT', $app->request->root());
//
//$app->run();

$uri = $_SERVER['REQUEST_URI'];
?>

<!DOCTYPE html>
<html>
<body>
<?php
switch ($uri) {
    case '/upload':
        require_once BASE_DIR . '/public/upload.php';
        break;
    case '/list':
        require_once BASE_DIR . '/public/list.php';
        break;
    case '/settings':
        require_once BASE_DIR . '/public/settings.php';
        break;
    default:
        require_once BASE_DIR . '/public/main.php';
}
?>

</body>
</html>
