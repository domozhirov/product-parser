<!DOCTYPE html>
<html>
<body>
<h1>Files</h1>
<ul>
<?php
$path = BASE_DIR . '/storage/files/';
if ($handle = opendir($path)) {

    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != ".." && strpos($entry, '.') !== 0) {
            $filemtime = date ("F d Y H:i:s.", filemtime("$path$entry") + 18000);
            echo '<li><a href="/files/' . $entry . '" target="_blank" download>' . $entry . '</a> ' . $filemtime . '<br></li>';
        }
    }

    closedir($handle);
}
?>
</ul>

<br>
<a href="/">Back</a>

</body>
</html>
