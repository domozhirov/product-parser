<?php
$target_dir    = BASE_DIR . "/storage/uploads/";
$target_file   = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk      = 1;
$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

// Check if file already exists
if (file_exists($target_file)) {
//    @unlink($target_file);
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 20000000) {
    echo "Sorry, your file is too large.<br>";
    $uploadOk = 0;
}
// Allow certain file formats
if ($imageFileType !== "csv") {
    echo "Sorry, only csv file is allowed.<br>";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.<br>";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        $filepath = $_FILES["fileToUpload"]["name"];
        echo "The file " . basename($filepath) . " has been uploaded.<br>";

        $mysqli = new mysqli("127.0.0.1", "root", "", "product_parser");
        $query = "SELECT * FROM settings";

        $result = $mysqli->query($query)->fetch_assoc();

        $settings = [
            'import' => $filepath,
            'export' => 'products' . date("Y-m-d H:i:s") . '.csv',
            'folders'=> explode("\n", $result['folders']),
            'maxlinechar' => 100000,
            'delim' => ';',
            'key' => $result['header_key'],
            'modkey' => 'is_kind : Модификация',
            'header' => [
                'article : Артикул',
                'name : Название',
                'body : Описание',
                'folder : Категория',
                'price : Цена',
                'cf_sebestoimost_ : Себестоимость',
            ],
            'mask' => json_decode($result['words_replace'], true),
        ];

        $filepath = $target_file;

        if (!file_exists($filepath) || !$file = fopen($filepath, 'r')) {
            message("File {$settings['import']} not exists!", 1);
        }

        $f = file_get_contents($filepath);

        if (!mb_check_encoding($f, 'UTF-8')) {
            $f = mb_convert_encoding($f, "utf-8", "windows-1251");;
            file_put_contents($filepath, $f);
        }

        unset($f);

        $total = count(file($filepath));

        message('Total products: ' . $total);

        $delim  = $settings['delim'];
        $header = fgetcsv($file, $settings['maxlinechar'], $delim);
        $hkey   = array_search($settings['key'], $header);
        $fkey   = array_search('Категория товара (полная)', $header);

        if ($hkey === false) {
            message('Key not found!', 1);
        }

        $prevrow = null;
        $is_mod  = false;
        $mask    = $settings['mask'];
        $folders = $settings['folders'];

        $newpath = BASE_DIR . '/storage/files/' . 'products.' . date("YmdHis") . '.csv';
        $newfile = fopen($newpath, "w");

        if (!$newfile) {
            message('Unable to open file!', 1);
        }

        fwrite($newfile, "\xEF\xBB\xBF");

        $header[$hkey] = $settings['modkey'];
        $hp_key        = array_search('pic_1', $header);

        array_splice($header, $hp_key);
        array_push($header, 'image : Картинки');

        $matics_left  = array_search('Erosklad', $header);
        $matics_right = array_search('Андрей', $header);

        foreach ($header as $key => $value) {
            if ($key <= $matics_left || $key > $matics_right) {
                if ($key) {
                    fwrite($newfile, $delim);
                }

                fwrite($newfile, '"' . $value . '"');
            }
        }

        $count = 1;

        $param_left  = array_search('Размер', $header);
        $param_right = array_search('Штук в упаковке', $header);

        while ($row = fgetcsv($file, $settings['maxlinechar'], $settings['delim'])) {
            $cvalue = ($row[$hkey]);
            $is_mod = $prevrow && isMod($mask, html_entity_decode(mb_strtolower($cvalue)), html_entity_decode(mb_strtolower($prevrow[$hkey - 5])));
            $images = array_slice($row, $hp_key);

            array_splice($row, $hp_key);

            array_push($row, parseImages($images));

            $prices = array_slice($row, $matics_left, $matics_right - $matics_left + 1);
            $prices_new = "";

            foreach ($dasdw = array_slice($header, $matics_left, $matics_right - $matics_left + 1) as $kkk => $val) {
                $price = (float)$prices[$kkk];

                if (empty($price)) {
                    continue;
                }

                if (!empty($prices_new) && $kkk) {
                    $prices_new .= "\r\n";
                }

                $prices_new .= "$val=$price";
            }

            $row[$matics_left] = $prices_new;

            array_splice($row, $matics_left + 1, $matics_right - $matics_left);

            fwrite($newfile, "\r\n");

            foreach ($row as $key => $cell) {
                if ($key >= $param_left && $key < $param_right && empty($cell)) {
                    $cell = '';
                }

                if ($folders && $key === $fkey) {
                    foreach ($folders as $folder) {
                        $folder = explode('|', $folder);

                        $cell = str_replace($folder[0], $folder[1], $cell);
                    }
                }

                if ($key === $hkey - 5) {
                    $cell = $is_mod ? '*' : '';
                }

                if ($key) {
                    fwrite($newfile, $delim);
                }

                if ($cell) {
                    $cell = str_replace('"', '&quot;', $cell);
                }

                if ($key === $fkey) {
                    fwrite($newfile, $cell);
                } else {
                    fwrite($newfile, '"' . $cell . '"');
                }
            }

            ++$count;

            $prevrow = $row;
        }

        fclose($file);
        fclose($newfile);

        $newfilename = basename($newpath);

        echo '<a href="' . '/files/' . $newfilename . '" target="_blank" download>' . $newfilename . '</a><br>';

        echo "\nDone\n";

    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}

echo '<br><a href="/">Back</a>';

function message(string $message, int $status = 0): bool
{
    print_r($message . '<br>');

    if ($status) {
        print_r('<br><a href="/">Back</a>');
        exit(1);
    }

    return true;
}

function isMod(array $mask, string $current, string $prev): bool
{
    $result = false;
    $regex  = '/[^а-яА-Яa-zA-Z]+/u';

    $current = str_replace('ё','е', $current);
    $prev    = str_replace('ё', 'е', $prev);

    $current = preg_replace($regex,'', $current);
    $prev = preg_replace($regex,'', $prev);

    foreach ($mask as $search) {
        $search = str_replace('ё','е', $search);
        $search = preg_replace('/\s/', '', $search);

        $current = str_replace(explode(',', mb_strtolower($search)), '', $current);
        $prev    = str_replace(explode(',', mb_strtolower($search)), '', $prev);
    }

    if ($current === $prev) {
        $result = true;
    }

    return $result;
}

function parseImages(array $images):string
{
    $result = '';
    $dir    = false;

    foreach ($images as $key => $image) {
        if (pathinfo($image, PATHINFO_EXTENSION)) {
            if ($key && !empty($result)) {
                $result .= ',';
            }

            if ($dir) {
                $image = explode(',', $image);

                foreach ($image as &$item) {
                    $item = $dir . $item;
                }

                $image = implode(',', $image);
            }

            $result .= $image;
        } elseif (!empty($image)) {
            $dir = $image;
        }
    }

    return $result;
}
