<?php

$mysqli = new mysqli("127.0.0.1", "root", "", "product_parser");

if ($mysqli->connect_errno) {
    printf("Connection failed: %s\n", $mysqli->connect_error);
    exit;
}

$query = "SELECT * FROM settings";

if ($result = $mysqli->query($query)) {

    $row = $result->fetch_assoc();

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $row['header_key']    = $_POST['header_key'];
        $row['words_replace'] = json_encode($_POST['words_replace'], JSON_UNESCAPED_UNICODE);
        $row['folders']       = str_replace("\r\n", "\n", $_POST['folders']);

        $sql = "UPDATE settings SET header_key='{$row['header_key']}', words_replace='{$row['words_replace']}', folders='{$row['folders']}'";

        $mysqli->query($sql);
    }

    echo "<h1>Settings</h1>";
    echo '<form action="/settings" method="post">';
    print_r('<div class="row" style="margin: 10px 0;">');
    print_r('<strong>Header cell value:</strong><br>');
    print_r('<input type="text" name="header_key" size="70" value="' . $row['header_key'] . '"><br>');
    print_r('</div>');

    print_r('<div class="row" style="margin: 10px 0;">');
    print_r('<strong>Mask:</strong><br>');

    print_r('<div style="margin: 5px 0;"><small>Add</small> <input type="text" id="new-word" size="70" value=""> <button class="button" id="add-new-word">+</button></div>');

    echo '<div style="margin: 10px 0; border-top: 1px solid #000; max-width: 500px;"></div>';

    if ($mask = json_decode($row['words_replace'], true)) {
        foreach ($mask as $value) {
            print_r('<div style="margin: 10px 0;"><input type="text" name="words_replace[]" size="70" value="' . $value . '"> <button class="remove">-</button></div>');
        }
    }
    print_r('</div>');

    print_r('<div class="row" style="margin: 10px 0;">');
    print_r('<strong>Folders:</strong><br>');

    echo '<div style="margin: 10px 0;"><textarea name="folders" cols="100" rows="30">' . $row['folders'] .  '</textarea></div>';

    print_r('</div>');

    echo "<button>Save</button>";

    echo "</form>";

    echo '<br><a href="/">Back</a><br>';

    $result->free();
}

$mysqli->close();
?>

<script>
    let button = document.getElementById('add-new-word');
    let field  = document.getElementById('new-word');
    let remove = document.querySelector('.remove');
    let parent, div;

    button.addEventListener('click', function (event) {
        event.preventDefault();

        parent = this.closest('.row');
        div    = document.createElement('div');
        div.style.margin = '10px 0';

        div.innerHTML = `
            <input type="text" value="${field.value}" name="words_replace[]" size="50">
            <button class="remove">-</button>
        `;

        field.value = '';

        parent.insertAdjacentElement('beforeend', div);
    });

    document.body.addEventListener('click', function (event) {
        if (event.target.classList.contains('remove')) {
            event.preventDefault();

            parent = event.target.closest('div');

            parent.closest('.row').removeChild(parent);
        }
    });

    let newFolderButton = document.getElementById('add-new-folder');
    let fieldq = document.getElementById('new-folder');

    newFolderButton.addEventListener('click', function (event) {
        event.preventDefault();

        parent = this.closest('.row');
        div    = document.createElement('div');
        div.style.margin = '10px 0';

        div.innerHTML = `
            <input type="text" value="${fieldq.value}" name="folders[]" size="50">
            <button class="remove">-</button>
        `;

        fieldq.value = '';

        parent.insertAdjacentElement('beforeend', div);
    });
</script>